public class FindDuplicateElementsInAnArrayUsingLoop {
    public static void main(String[] args) {
        int[] elements = {10,15,23,42,18,67,45,15,10};

        for (int i = 0; i < elements.length; i++) {
            for (int j = i+1; j < elements.length; j++) {
                if(elements[i]==elements[j]){
                    System.out.println(elements[i] +" is duplicated");
                }
            }
        }
    }
}