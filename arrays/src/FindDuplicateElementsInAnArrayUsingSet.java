import java.util.HashSet;
import java.util.Set;

public class FindDuplicateElementsInAnArrayUsingSet {
    public static void main(String[] args) {
        int[] elements = {10,15,23,42,18,67,45,15,10};
        Set<Integer> elementSet = new HashSet<>();
        for (int i = 0; i < elements.length; i++) {
            boolean status = elementSet.add(elements[i]);
            if(!status){
                System.out.println("duplicate element found::"+elements[i]);
            }
        }
    }
}