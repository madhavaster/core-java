import java.util.ArrayList;
import java.util.List;

public class CollectionExample2 {
    public static void main(String[] args) {
        List<String> names = new ArrayList<>();
        names.add("madhava");
        names.add("swapna");

        List<String> kids = new ArrayList<>();
        kids.add("hemanth");
        kids.add("manas");

        names.addAll(kids);

        names.forEach(System.out::println);
    }
}
