import java.util.ArrayList;
import java.util.List;

public class CollectionExample {
    public static void main(String[] args) {
        List<String> names = new ArrayList<>();
        names.add("madhava");
        names.add("swapna");
        names.add("hemanth");
        names.add("manas");

        List<String> kids = new ArrayList<>();
        kids.add("hemanth");
        kids.add("manas");

        String[] strings = names.toArray(new String[names.size()]);
        for (String name :
                strings) {
            System.out.println(name);
        }
    }
}
