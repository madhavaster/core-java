import java.util.Scanner;

public class Calculator {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter first number:");
        int i1 = scanner.nextInt();
        System.out.println("Enter second number:");
        int i2 = scanner.nextInt();
        System.out.println("Enter the operation to be performed(+,-,*,/):::");
        String operation = scanner.next();
        int result = calculate(i1, i2, operation);
        System.out.println("Result of "+i1+" "+operation+" "+i2+" is : " + result);
        scanner.close();
    }

    private static int calculate(int i1, int i2, String operation) {
        int result = 0;
        switch (operation) {
            case "+":
                result = i1 + i2;
                break;
            case "-":
                result = i1 - i2;
                break;
            case "*":
                result = i1 * i2;
                break;
            case "/":
                result = i1 / i2;
                break;
        }
        return result;
    }
}
